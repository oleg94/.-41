<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require 'libs/Smarty.class.php';
$smarty = new Smarty();

$request = $_GET;

//$smarty->debugging = true;
//$smarty->caching = true;
//$smarty->cache_lifetime = 120;

if (!empty($request['dispath'])) {
	$dispath = $request['dispath'];
} else {
	$dispath = 'index';
}

$menu = array(
	'0' => array(
		'name' => 'Main',
		'url' => 'index.php?dispath=index',
		'active' => $dispath == 'index',
	),
	'1' => array(
		'name' => 'Feedback',
		'url' => 'index.php?dispath=feedback',
		'active' => $dispath == 'feedback',
	),
);

$smarty->assign("menu", $menu);
$smarty->assign("dispath", $dispath);

$smarty->display("base.tpl");

?>