<nav class="navbar navbar-default navbar-static-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php?dispath=index">Project name</a>
    </div>

    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        {foreach $menu as $item}
        <li class="{if $item['active']}active{/if}"><a href="{$item['url']}">{$item['name']}</a></li>
        {/foreach}
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>